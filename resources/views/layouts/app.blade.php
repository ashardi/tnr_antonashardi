<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name') }}</title>

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link href="{{ asset('css/datatables.bootstrap.css') }}" rel="stylesheet">
</head>
<body>
    <div id="app">
        <nav class="navbar navbar-expand-md navbar-light navbar-laravel">
            <div class="container">
                <a class="navbar-brand" href="{{ url('/') }}">
                   {{ config('app.name') }}
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->
                    <ul class="navbar-nav mr-auto">

                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ml-auto">
                        <!-- Authentication Links -->
                        <!-- @if(!Auth::check())
                            <li><a class="nav-link" href="{{ url('/login') }}">Login</a></li>
                            <li><a class="nav-link" href="{{ url('/register') }}">Register</a></li>
                        @else
                            <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="{{ url('/logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        Logout
                                    </a>

                                    <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </div>
                            </li>
                        @endif -->
                    </ul>
                </div>
            </div>
        </nav>

        <main class="py-4">
            @yield('content')
        </main>
    </div>

    <!-- Scripts -->
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="{{ asset('js/app.js') }}"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script src="{{ asset('js/jquery.dataTables.min.js') }}"></script>
    <script type="text/javascript">
        $( function() {
            $( ".datepicker" ).datepicker({
                onSelect: function(date) {
                    // console.log('asddate', date);
                    $(".datepicker").val(date);
                },
                dateFormat: "yy-mm-dd",
                changeMonth: true,
                changeYear: true
            });
        } );
        // Movies Datatables
        $(function() {
            var oTable = $('#tablemovies').DataTable({
                processing: true,
                serverSide: true,
                ajax: {
                    url: '{{ url("datamovies") }}'
                },
                columns: [
                    {data: 'title', name: 'title'},
                    {data: 'genre', name: 'genre'},
                    {data: 'released_date', name: 'released_date'},
                    {data: 'action', name: 'action', orderable: false},
                ],
            });
        });
        // Members Datatables
        $(function() {
            var oTable = $('#tablemembers').DataTable({
                processing: true,
                serverSide: true,
                ajax: {
                    url: '{{ url("datamembers") }}'
                },
                columns: [
                    {data: 'name', name: 'name'},
                    {data: 'address', name: 'address'},
                    {data: 'joined_date', name: 'joined_date'},
                    {data: 'is_active', name: 'is_active'},
                    {data: 'action', name: 'action', orderable: false},
                ],
            });
        });
        // Lending Datatables
        $(function() {
            var oTable = $('#tablelending').DataTable({
                processing: true,
                serverSide: true,
                ajax: {
                    url: '{{ url("datalending") }}'
                },
                columns: [
                    {data: 'movie_id', name: 'movie_id'},
                    {data: 'member_id', name: 'member_id'},
                    {data: 'lending_date', name: 'lending_date'},
                    {data: 'returned_date', name: 'returned_date'},
                    {data: 'lateness_charge', name: 'lateness_charge'},
                    {data: 'action', name: 'action', orderable: false},
                ],
            });
        });
        function clearForm(form) {
            $('#'+form).find("input[type=text], textarea, select").val("");
        }
    </script>
</body>
</html>
