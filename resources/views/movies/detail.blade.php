<div class="table-responsive">
    <table class="table">
        <tbody>
            <tr><th style="width: 120px;"> Title </th><td>: <span id="title"></span> </td></tr>
            <tr><th> Genre </th><td>: <span id="genre"></span> </td></tr>
            <tr><th> Released Date </th><td>: <span id="released_date"></span> </td></tr>
        </tbody>
    </table>
    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
</div>