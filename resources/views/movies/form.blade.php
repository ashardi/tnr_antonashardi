<div class="form-group {{ $errors->has('title') ? 'has-error' : ''}}">
    <label for="title" class="control-label">{{ 'Title' }}</label>
    <input class="form-control" name="title" type="text" id="title" value="{{ ($movie) ? $movie->title : '' }}" required>
    {!! $errors->first('title', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('genre') ? 'has-error' : ''}}">
    <label for="genre" class="control-label">{{ 'Genre' }}</label>
    <select class="form-control" name="genre" id="genre" required>
        <option value="">Choose Genre ...</option>
        <option value="Action" {{ ($movie) ? (($movie->genre == 'Action') ? 'selected' : '' ) : '' }}>Action</option>
        <option value="Comedy" {{ ($movie) ? (($movie->genre == 'Comedy') ? 'selected' : '' ) : '' }}>Comedy</option>
        <option value="Drama" {{ ($movie) ? (($movie->genre == 'Drama') ? 'selected' : '' ) : '' }}>Drama</option>
        <option value="Horror" {{ ($movie) ? (($movie->genre == 'Horror') ? 'selected' : '' ) : '' }}>Horror</option>
        <option value="Thriller" {{ ($movie) ? (($movie->genre == 'Thriller') ? 'selected' : '' ) : '' }}>Thriller</option>
    </select>
    {!! $errors->first('genre', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('released_date') ? 'has-error' : ''}}">
    <label for="released_date" class="control-label">{{ 'Released Date' }}</label>
    <input class="datepicker form-control" name="released_date" type="text" id="released_date" value="{{ ($movie) ? $movie->released_date : '' }}" autocomplete="off" required>
    {!! $errors->first('released_date', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group">
    <input class="btn btn-primary" type="submit" value="{{ $formMode === 'edit' ? 'Update' : 'Create' }}">
    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
</div>