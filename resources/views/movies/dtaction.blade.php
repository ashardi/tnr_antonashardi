<!-- <a href="{{ url('/movies/' . $id) }}" title="View Movie"><button class="btn btn-info btn-sm"><i class="fa fa-eye" aria-hidden="true"></i></button></a> -->
<a href="javascript:void(0)" onclick="loadShowMovie({{$id}})" title="View Movie"><button class="btn btn-info btn-sm"><i class="fa fa-eye" aria-hidden="true"></i></button></a>
<!-- <a href="{{ url('/movies/' . $id . '/edit') }}" title="Edit Movie"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></button></a> -->
<a href="javascript:void(0)" onclick="loadEditMovie({{$id}})" title="Edit Movie"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></button></a>

<form method="POST" action="{{ url('/movies' . '/' . $id) }}" accept-charset="UTF-8" style="display:inline">
    {{ method_field('DELETE') }}
    {{ csrf_field() }}
    <button type="submit" class="btn btn-danger btn-sm" title="Delete Movie" onclick="return confirm(&quot;Confirm delete?&quot;)"><i class="fa fa-trash-o" aria-hidden="true"></i></button>
</form>
<script>
    function loadShowMovie(id) {
        var url = "{{ url('getmovie', '__id') }}";
        var urlForm = "{{ url('movies', '__id') }}";
        url = url.replace('__id', id);
        urlForm = urlForm.replace('__id', id);
        $.get( url, function( data ) {
            $('#showModal').modal('show');
            $(".modal-body #title").html(data.title);
            $(".modal-body #genre").html(data.genre);
            $(".modal-body #released_date").html($.datepicker.formatDate('dd-mm-yy', new Date(data.released_date)));
        });
    };
    function loadEditMovie(id) {
        var url = "{{ url('getmovie', '__id') }}";
        var urlForm = "{{ url('movies', '__id') }}";
        url = url.replace('__id', id);
        urlForm = urlForm.replace('__id', id);
        $.get( url, function( data ) {
            $('#editModal').modal('show');
            $("#editMovieForm").attr('action', urlForm);
            $(".modal-body #title").val(data.title);
            $(".modal-body #genre").val(data.genre);
            $(".modal-body #released_date").val(data.released_date);
        });
    };
</script>