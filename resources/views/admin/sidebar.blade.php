<div class="col-md-3">
    <div class="card">
        <div class="card-header">
            Menu
        </div>

        <div class="card-body">
            <ul class="nav flex-column">
                <li class="nav-item">
                    <a class="nav-link" href="{{ url('/movies') }}">Movies</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{ url('/members') }}">Members</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{ url('/lending') }}">Lending / Returning</a>
                </li>
            </ul>
        </div>
    </div>
</div>
