<a href="javascript:void(0)" onclick="loadShowMember({{$id}})" title="View Member"><button class="btn btn-info btn-sm"><i class="fa fa-eye" aria-hidden="true"></i></button></a>
<a href="javascript:void(0)" onclick="loadEditMember({{$id}})" title="Edit Member"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></button></a>

<form method="POST" action="{{ url('/members' . '/' . $id) }}" accept-charset="UTF-8" style="display:inline">
    {{ method_field('DELETE') }}
    {{ csrf_field() }}
    <button type="submit" class="btn btn-danger btn-sm" title="Delete Member" onclick="return confirm(&quot;Confirm delete?&quot;)"><i class="fa fa-trash-o" aria-hidden="true"></i></button>
</form>
<script>
    function loadShowMember(id) {
        var url = "{{ url('getmember', '__id') }}";
        var urlForm = "{{ url('member', '__id') }}";
        url = url.replace('__id', id);
        urlForm = urlForm.replace('__id', id);
        $.get( url, function( data ) {
            $('#showModal').modal('show');
            $(".modal-body #name").html(data.name);
            $(".modal-body #age").html(data.age);
            $(".modal-body #address").html(data.address);
            $(".modal-body #phone").html(data.phone);
            $(".modal-body #id_no").html(data.id_no);
            $(".modal-body #joined_date").html($.datepicker.formatDate('dd-mm-yy', new Date(data.joined_date)));
            var is_activeVal = 'Yes';
            if(data.is_active == '0') is_activeVal = 'No'; 
            $(".modal-body #is_active").html(is_activeVal);
        });
    };
    function loadEditMember(id) {
        var url = "{{ url('getmember', '__id') }}";
        var urlForm = "{{ url('members', '__id') }}";
        url = url.replace('__id', id);
        urlForm = urlForm.replace('__id', id);
        $.get( url, function( data ) {
            console.log("data", data);
            $('#editModal').modal('show');
            $("#editMemberForm").attr('action', urlForm);
            $(".modal-body #name").val(data.name);
            $(".modal-body #age").val(data.age);
            $(".modal-body #address").val(data.address);
            $(".modal-body #phone").val(data.phone);
            $(".modal-body #id_no").val(data.id_no);
            $(".modal-body #joined_date").val(data.joined_date);
            if(data.is_active == '1') $(".modal-body #is_activeYes").prop('checked', true);
            if(data.is_active == '0') $(".modal-body #is_activeNo").prop('checked', true);
        });
    };
</script>