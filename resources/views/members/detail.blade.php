<div class="table-responsive">
    <table class="table">
        <tbody>
            <tr><th style="width: 120px;"> Name </th><td>: <span id="name"></span> </td></tr>
            <tr><th> Age </th><td>: <span id="age"></span> </td></tr>
            <tr><th> Address </th><td>: <span id="address"></span> </td></tr>
            <tr><th> Telephone </th><td>: <span id="phone"></span> </td></tr>
            <tr><th> ID Number </th><td>: <span id="id_no"></span> </td></tr>
            <tr><th> Date of Joined </th><td>: <span id="joined_date"></span> </td></tr>
            <tr><th> Active? </th><td>: <span id="is_active"></span> </td></tr>
        </tbody>
    </table>
    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
</div>