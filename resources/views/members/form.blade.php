<div class="form-group {{ $errors->has('name') ? 'has-error' : ''}}">
    <label for="name" class="control-label">{{ 'Name' }}</label>
    <input class="form-control" name="name" type="text" id="name" value="{{ ($member) ? $member->name : '' }}" required>
    {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('age') ? 'has-error' : ''}}">
    <label for="age" class="control-label">{{ 'Age' }}</label>
    <input class="form-control" name="age" type="number" id="age" value="{{ ($member) ? $member->age : '' }}" requried>
    {!! $errors->first('age', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('address') ? 'has-error' : ''}}">
    <label for="address" class="control-label">{{ 'Address' }}</label>
    <textarea class="form-control" rows="5" name="address" type="textarea" id="address" required>{{ ($member) ? $member->address : '' }}</textarea>
    {!! $errors->first('address', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('phone') ? 'has-error' : ''}}">
    <label for="phone" class="control-label">{{ 'Phone' }}</label>
    <input class="form-control" name="phone" type="text" id="phone" value="{{ ($member) ? $member->phone : '' }}" required>
    {!! $errors->first('phone', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('id_no') ? 'has-error' : ''}}">
    <label for="id_no" class="control-label">{{ 'ID Number' }}</label>
    <input class="form-control" name="id_no" type="text" id="id_no" value="{{ ($member) ? $member->id_no : '' }}" required>
    {!! $errors->first('id_no', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('joined_date') ? 'has-error' : ''}}">
    <label for="joined_date" class="control-label">{{ 'Joined Date' }}</label>
    <input class="datepicker form-control" name="joined_date" type="text" id="joined_date" value="{{ ($member) ? $member->joined_date : '' }}" required autocomplete="off">
    {!! $errors->first('joined_date', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('is_active') ? 'has-error' : ''}}">
    <label for="is_active" class="control-label">{{ 'Is Active' }}</label>
    <div class="form-check">
        <input class="form-check-input" type="radio" name="is_active" id="is_activeYes" value="1" {{ ($member) ? (($member->is_active == '1') ? 'checked' : '') : '' }}>
        <label class="form-check-label" for="is_activeYes">
            Yes
        </label>
        </div>
        <div class="form-check">
        <input class="form-check-input" type="radio" name="is_active" id="is_activeNo" value="0" {{ ($member) ? (($member->is_active == '0') ? 'checked' : '') : '' }}>
        <label class="form-check-label" for="is_activeNo">
            No
        </label>
    </div>
    {!! $errors->first('is_active', '<p class="help-block">:message</p>') !!}
</div>


<div class="form-group">
    <input class="btn btn-primary" type="submit" value="{{ $formMode === 'edit' ? 'Update' : 'Create' }}">
    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
</div>