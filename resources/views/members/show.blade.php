@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            @include('admin.sidebar')

            <div class="col-md-9">
                <div class="card">
                    <div class="card-header">Member: {{ $member->name }}</div>
                    <div class="card-body">

                        <a href="{{ url('/members') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
                        <a href="{{ url('/members/' . $member->id . '/edit') }}" title="Edit Member"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</button></a>

                        <form method="POST" action="{{ url('members' . '/' . $member->id) }}" accept-charset="UTF-8" style="display:inline">
                            {{ method_field('DELETE') }}
                            {{ csrf_field() }}
                            <button type="submit" class="btn btn-danger btn-sm" title="Delete Member" onclick="return confirm(&quot;Confirm delete?&quot;)"><i class="fa fa-trash-o" aria-hidden="true"></i> Delete</button>
                        </form>
                        <br/>
                        <br/>

                        <div class="table-responsive">
                            <table class="table">
                                <tbody>
                                    <tr><th style="width: 120px;"> Name </th><td>: {{ $member->name }} </td></tr>
                                    <tr><th> Age </th><td>: {{ $member->age }} </td></tr>
                                    <tr><th> Address </th><td>: {{ $member->address }} </td></tr>
                                    <tr><th> Telephone </th><td>: {{ $member->phone }} </td></tr>
                                    <tr><th> ID Number </th><td>: {{ $member->id_no }} </td></tr>
                                    <tr><th> Date of Joined </th><td>: {{ \Carbon\Carbon::parse($member->joined_date)->format('d-m-Y')}} </td></tr>
                                    <tr><th> Active? </th><td>: {{ ($member->is_active == '1') ? 'Yes' : 'No' }} </td></tr>
                                </tbody>
                            </table>
                        </div>
                        <br>

                        <h4>Lending History</h4>

                        <div class="table-responsive">
                            <table class="table">
                                <tbody>
                                    <tr>
                                        <th>Lending Date</th>
                                        <th>Movie</th>
                                    </tr>
                                    @foreach($member->renting as $rent)
                                    <tr>
                                        <td>{{ \Carbon\Carbon::parse($rent->lending_date)->format('d-m-Y')}}</th>
                                        <td>{{ $rent->movie->title }}</th>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
