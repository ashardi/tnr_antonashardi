@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            @include('admin.sidebar')

            <div class="col-md-9">
                <div class="card">
                    <div class="card-header">Lending: <b>{{ $lending->movie->title }}</b> lend by {{ $lending->member->name }} at {{ \Carbon\Carbon::parse($lending->lending_date)->format('d-m-Y')}}</div>
                    <div class="card-body">

                        <a href="{{ url('/lending') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
                        <!-- <a href="{{ url('/lending/' . $lending->id . '/edit') }}" title="Edit Lending"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</button></a> -->

                        <form method="POST" action="{{ url('lending' . '/' . $lending->id) }}" accept-charset="UTF-8" style="display:inline">
                            {{ method_field('PATCHs') }}
                            {{ csrf_field() }}
                            <button type="submit" class="btn btn-success btn-sm" title="Return Movie" onclick="return confirm(&quot;Confirm return movie?&quot;)"><i class="fa fa-undo" aria-hidden="true"></i> Return Movie</button>
                        </form>
                        <br/>
                        <br/>

                        <div class="table-responsive">
                            <table class="table">
                                <tbody>
                                    <tr><th style="width: 150px;"> Movie </th><td>: {{ $lending->movie->title }} </td></tr>
                                    <tr><th> Member </th><td>: {{ $lending->member->name }} </td></tr>
                                    <tr><th> Lending Date </th><td>: {{ \Carbon\Carbon::parse($lending->lending_date)->format('d-m-Y')}} </td></tr>
                                    <tr><th> Return Date </th><td>: {{ \Carbon\Carbon::parse($lending->returned_date)->format('d-m-Y')}} </td></tr>
                                    <tr><th> Lateness Charge </th><td>: {{ $lending->lateness_charge }} </td></tr>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
