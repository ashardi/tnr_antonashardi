<div class="form-group {{ $errors->has('movie_id') ? 'has-error' : ''}}">
    <label for="movie_id" class="control-label">{{ 'Movie Id' }}</label>
    <select class="form-control" name="movie_id" id="movie_id" required>
        <option value="">Choose movie ...</option>
        @foreach($movies as $movie)
        <option value="{{$movie->id}}" {{ ($lend) ? (($lend->movie_id == $movie->id) ? 'selected' : '' )  : '' }}>{{$movie->title}} - Genre: {{$movie->genre}}</option>
        @endforeach
    </select>
    {!! $errors->first('movie_id', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('member_id') ? 'has-error' : ''}}">
    <label for="member_id" class="control-label">{{ 'Member Id' }}</label>
    <select class="form-control" name="member_id" id="member_id" required>
        <option value="">Choose member ...</option>
        @foreach($members as $member)
        <option value="{{$member->id}}" {{ ($lend) ? (($lend->member_id == $member->id) ? 'selected' : '' )  : '' }}>{{$member->name}}</option>
        @endforeach
    </select>
    {!! $errors->first('member_id', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('lending_date') ? 'has-error' : ''}}">
    <label for="lending_date" class="control-label">{{ 'Lending Date' }}</label>
    <input class="datepicker form-control" name="lending_date" type="text" id="lending_date" value="{{ ($lend) ? $lend->lending_date : ''}}" required autocomplete="off">
    {!! $errors->first('lending_date', '<p class="help-block">:message</p>') !!}
</div>
<!-- <div class="form-group {{ $errors->has('returned_date') ? 'has-error' : ''}}">
    <label for="returned_date" class="control-label">{{ 'Returned Date' }}</label>
    <input class="datepicker form-control" name="returned_date" type="text" id="returned_date" value="{{ ($lend) ? $lend->returned_date : ''}}" required>
    {!! $errors->first('returned_date', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('lateness_charge') ? 'has-error' : ''}}">
    <label for="lateness_charge" class="control-label">{{ 'Lateness Charge' }}</label>
    <input class="form-control" name="lateness_charge" type="number" id="lateness_charge" value="{{ ($lend) ? $lend->lateness_charge : '0'}}" required>
    {!! $errors->first('lateness_charge', '<p class="help-block">:message</p>') !!}
</div> -->


<div class="form-group">
    <input class="btn btn-primary" type="submit" value="{{ $formMode === 'edit' ? 'Update' : 'Create' }}">
</div>
