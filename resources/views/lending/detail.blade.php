<div class="table-responsive">
    <table class="table">
        <tbody>
            <tr><th style="width: 150px;"> Movie </th><td>: <span id="movie-title"></span> </td></tr>
            <tr><th> Member </th><td>: <span id="member-name"></span> </td></tr>
            <tr><th> Lending Date </th><td>: <span id="lending_date"></span> </td></tr>
            <tr><th> Return Date </th><td>: <span id="returned_date"></span> </td></tr>
            <tr><th> Lateness Charge </th><td>: <span id="lateness_charge"></span> </td></tr>
        </tbody>
    </table>
</div>