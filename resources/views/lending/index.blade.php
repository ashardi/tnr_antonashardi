@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            @include('admin.sidebar')

            <div class="col-md-9">
                <div class="card">
                    <div class="card-header">Lending / Returning</div>
                    <div class="card-body">
                    <button type="button" class="btn btn-success btn-sm" data-toggle="modal" data-target="#addNewModal" onclick="clearForm('addLendingForm')">
                            <i class="fa fa-plus" aria-hidden="true"></i> Add New
                        </button>

                        <!-- Modal -->
                        <div class="modal fade" id="addNewModal" tabindex="-1" role="dialog" aria-labelledby="addNewModalTitle" aria-hidden="true">
                            <div class="modal-dialog modal-dialog-centered" role="document">
                                <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalCenterTitle">Add New</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                <form method="POST" id="addLendingForm" action="{{ url('/lending') }}" accept-charset="UTF-8" class="form-horizontal" enctype="multipart/form-data">
                                    {{ csrf_field() }}

                                    @include ('lending.form', ['formMode' => 'create'])

                                </form>
                                </div>
                                </div>
                            </div>
                        </div>

                        <div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="editModalTitle" aria-hidden="true">
                            <div class="modal-dialog modal-dialog-centered" role="document">
                                <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalCenterTitle">Add New</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                <form method="POST" id="editLendingForm" accept-charset="UTF-8" class="form-horizontal" enctype="multipart/form-data">
                                    {{ method_field('PATCH') }}
                                    {{ csrf_field() }}

                                    @include ('lending.form', ['formMode' => 'edit'])

                                </form>
                                </div>
                                </div>
                            </div>
                        </div>

                        <div class="modal fade" id="showModal" tabindex="-1" role="dialog" aria-labelledby="showModalTitle" aria-hidden="true">
                            <div class="modal-dialog modal-dialog-centered" role="document">
                                <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalCenterTitle">Detail</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    @include ('lending.detail')
                                </div>
                                </div>
                            </div>
                        </div>

                        <!-- <form method="GET" action="{{ url('/lending') }}" accept-charset="UTF-8" class="form-inline my-2 my-lg-0 float-right" role="search">
                            <div class="input-group">
                                <input type="text" class="form-control" name="search" placeholder="Search..." value="{{ request('search') }}">
                                <span class="input-group-append">
                                    <button class="btn btn-secondary" type="submit">
                                        <i class="fa fa-search"></i>
                                    </button>
                                </span>
                            </div>
                        </form> -->

                        <br/>
                        <br/>
                        <div class="alert alert-info">Every movie title have 1 day rent time, if returned more than 1 day will add late charge each day until returned.</div>
                        <table class="table" id="tablelending">
                            <thead>
                                <tr>
                                    <th>Movie</th>
                                    <th>Member</th>
                                    <th>Lend Date</th>
                                    <th>Return Date</th>
                                    <th>Late Charge</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                        </table>
                        <!-- <div class="table-responsive">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>#</th><th>Movie</th>
                                        <th>Member Id</th>
                                        <th>Lend Date</th>
                                        <th>Return Date</th>
                                        <th>Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @foreach($lending as $item)
                                    <tr>
                                        <td>{{ $loop->iteration }}</td>
                                        <td>{{ $item->movie->title }}</td>
                                        <td>{{ $item->member->name }}</td>
                                        <td>{{ \Carbon\Carbon::parse($item->lending_date)->format('d-m-Y')}}</td>
                                        <td>{{ \Carbon\Carbon::parse($item->returned_date)->format('d-m-Y')}}</td>
                                        <td>
                                            <a href="{{ url('/lending/' . $item->id) }}" title="View Lending"><button class="btn btn-info btn-sm"><i class="fa fa-eye" aria-hidden="true"></i></button></a>
                                            <a href="{{ url('/lending/' . $item->id . '/edit') }}" title="Edit Lending"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></button></a>

                                            <form method="POST" action="{{ url('/lending' . '/' . $item->id) }}" accept-charset="UTF-8" style="display:inline">
                                                {{ method_field('PATCH') }}
                                                {{ csrf_field() }}
                                                <button type="submit" class="btn btn-success btn-sm" title="Return Movie" onclick="return confirm(&quot;Confirm return movie?&quot;)"><i class="fa fa-undo" aria-hidden="true"></i></button>
                                            </form>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            <div class="pagination-wrapper"> {!! $lending->appends(['search' => Request::get('search')])->render() !!} </div>
                        </div> -->

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
