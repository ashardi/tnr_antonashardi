<a href="javascript:void(0)" onclick="loadShowLending({{$id}})" title="View Lending"><button class="btn btn-info btn-sm"><i class="fa fa-eye" aria-hidden="true"></i></button></a>
<!-- <a href="{{ url('/lending/' . $id . '/edit') }}" title="Edit Lending"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></button></a> -->

@if ($status == '1')
<form method="POST" action="{{ url('/lending' . '/' . $id) }}" accept-charset="UTF-8" style="display:inline">
    {{ method_field('PATCH') }}
    {{ csrf_field() }}
    <button type="submit" class="btn btn-success btn-sm" title="Return Movie" onclick="return confirm(&quot;Confirm return movie?&quot;)"><i class="fa fa-undo" aria-hidden="true"></i></button>
</form>
@endif
<script>w
    function loadShowLending(id) {
        var url = "{{ url('getlending', '__id') }}";
        var urlForm = "{{ url('lending', '__id') }}";
        url = url.replace('__id', id);
        urlForm = urlForm.replace('__id', id);
        $.get( url, function( data ) {
            console.log("data", data);
            $('#showModal').modal('show');
            $(".modal-body #movie-title").html(data.movie.title);
            $(".modal-body #member-name").html(data.member.name);
            $(".modal-body #lending_date").html($.datepicker.formatDate('dd-mm-yy', new Date(data.lending_date)));
            $(".modal-body #returned_date").html($.datepicker.formatDate('dd-mm-yy', new Date(data.returned_date)));
            $(".modal-body #lateness_charge").html(data.lateness_charge);
        });
    };
    function loadEditLending(id) {
        var url = "{{ url('getlending', '__id') }}";
        var urlForm = "{{ url('lending', '__id') }}";
        url = url.replace('__id', id);
        urlForm = urlForm.replace('__id', id);
        $.get( url, function( data ) {
            console.log("data", data);
            $('#editModal').modal('show');
            $("#editLendingForm").attr('action', urlForm);
            $(".modal-body #name").val(data.name);
            $(".modal-body #age").val(data.age);
            $(".modal-body #address").val(data.address);
            $(".modal-body #phone").val(data.phone);
            $(".modal-body #id_no").val(data.id_no);
            $(".modal-body #joined_date").val(data.joined_date);
            if(data.is_active == '1') $(".modal-body #is_activeYes").prop('checked', true);
            if(data.is_active == '0') $(".modal-body #is_activeNo").prop('checked', true);
        });
    };
</script>