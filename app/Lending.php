<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Lending extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'lendings';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['movie_id', 'member_id', 'lending_date', 'returned_date', 'lateness_charge'];

    public function movie()
    {
        return $this->belongsTo('App\Movie');
    }
    
    public function member()
    {
        return $this->belongsTo('App\Member');
    }
}
