<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Lending;
use App\Movie;
use App\Member;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;

class LendingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $lend = array();
        $movies = Movie::where('status', '0')->get();
        $members = Member::where('is_active', '1')->get();
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $lending = Lending::where('movie_id', 'LIKE', "%$keyword%")
                ->where('status', '1')
                ->orWhere('member_id', 'LIKE', "%$keyword%")
                ->orWhere('member_id', 'LIKE', "%$keyword%")
                ->orWhere('lending_date', 'LIKE', "%$keyword%")
                ->orWhere('returned_date', 'LIKE', "%$keyword%")
                ->orWhere('lateness_charge', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $lending = Lending::latest()->where('status', '1')->paginate($perPage);
        }

        // $lends = Lending::where('id', 1)->movie();
        // echo '<pre>';print_r($lends);echo '</pre>'; exit;

        return view('lending.index', compact('lending', 'lend', 'movies', 'members'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $lending = array();
        $movies = Movie::where('status', '0')->get();
        $members = Member::where('is_active', '1')->get();

        return view('lending.create', compact('lending', 'movies', 'members'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        
        $requestData = $request->all();
        if(Lending::create($requestData)) {
            // Update movie status to lend
            $movie = Movie::findOrFail($requestData['movie_id']);
            $movie->update(array('status'=>'1'));
        }

        return redirect('lending')->with('flash_message', 'Lending added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $lending = Lending::findOrFail($id);

        return view('lending.show', compact('lending'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $lending = Lending::findOrFail($id);
        $movies = Movie::get();
        $members = Member::where('is_active', '1')->get();

        return view('lending.edit', compact('lending', 'movies', 'members'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    // public function update(Request $request, $id)
    public function update($id)
    {
        
        // $requestData = $request->all();
        
        // $lending = Lending::findOrFail($id);
        // $lending->update($requestData);
        $lending = Lending::findOrFail($id);
        if($lending) {
            // Update movie status to lend
            $movie = Movie::findOrFail($lending->movie_id);
            $movie->update(array('status'=>'0'));

            // Count late charge
            $lend_interval = 0;
            $late_charge = 0;
            $late_criteria = 1; // Lend allowed only for 1 day
            $lend_date = date_create($lending->lending_date);
            $return_date = date_create(date('Y-m-d'));

            // Handle returning on the same day
            if($lend_date < $return_date) {
                $lend_interval = date_diff($return_date, $lend_date)->days;
            }

            // Count late charte
            if($lend_interval >= 2) {
                $late_charge = $lend_interval - $late_criteria;
            }

            // Handle minus counting of late charges
            if($late_charge < 0) $late_charge = 0;

            Lending::where('id', $id)->update(array('status'=>'0', 'returned_date'=>date('Y-m-d'), 'lateness_charge'=>$late_charge));
        }

        return redirect('lending')->with('flash_message', 'Lending updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        $lending = Lending::findOrFail($id);
        if($lending) {
            // Update movie status to lend
            $movie = Movie::findOrFail($lending->movie_id);
            $movie->update(array('status'=>'0'));

            Lending::destroy($id);
        }

        return redirect('lending')->with('flash_message', 'Lending deleted!');
    }

    /**
     * Get the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function getLending($id) {
        return Lending::with('movie')->with('member')->findOrFail($id);
    }

    public function dataLending() {
        return Datatables::of(Lending::query())
        ->editColumn('movie_id', function(Lending $lending) { return $lending->movie->title; })
        ->editColumn('member_id', function(Lending $lending) { return $lending->member->name; })
        ->editColumn('lending_date', function(Lending $lending) { return date_format(date_create($lending->lending_date), 'd-m-Y'); })
        ->editColumn('returned_date', function(Lending $lending) { if($lending->returned_date) return date_format(date_create($lending->returned_date), 'd-m-Y'); })
        ->addColumn('action', 'lending.dtaction')
        // ->order(function ($query) { $query->orderBy('lending_date', 'desc'); })
        ->toJson();
    }
}
