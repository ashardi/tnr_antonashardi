<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Movie;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;

class MoviesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $movie = array();
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $movies = Movie::where('title', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $movies = Movie::orderBy('title', 'asc')->paginate($perPage);
        }

        return view('movies.index', compact('movies', 'movie'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $movie = array();

        return view('movies.create', compact('movie'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        
        $requestData = $request->all();
        
        Movie::create($requestData);

        return redirect('movies')->with('flash_message', 'Movie added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $movie = Movie::findOrFail($id);

        // $lends = $movie->lender;
        // foreach($lends as $lend) {
        //     echo $lend->lending_date;
        // }

        return view('movies.show', compact('movie'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $movie = Movie::findOrFail($id);

        return view('movies.edit', compact('movie'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        
        $requestData = $request->all();
        
        $movie = Movie::findOrFail($id);
        $movie->update($requestData);

        return redirect('movies')->with('flash_message', 'Movie updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Movie::destroy($id);

        return redirect('movies')->with('flash_message', 'Movie deleted!');
    }

    /**
     * Get the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function getMovie($id) {
        return Movie::findOrFail($id);
    }

    public function dataMovies() {
        return Datatables::of(Movie::query())
        ->editColumn('released_date', function(Movie $movie) { return date_format(date_create($movie->released_date), 'd-m-Y'); })
        ->addColumn('action', 'movies.dtaction')
        // ->order(function ($query) { $query->orderBy('title', 'asc');})
        ->toJson();
    }
}
