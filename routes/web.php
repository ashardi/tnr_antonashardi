<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'MoviesController@index');
Route::get('/getmovie/{id}', 'MoviesController@getMovie');
Route::get('/datamovies', 'MoviesController@dataMovies');
Route::get('/getmember/{id}', 'MembersController@getMember');
Route::get('/datamembers', 'MembersController@dataMembers');
Route::get('/getlending/{id}', 'LendingController@getLending');
Route::get('/datalending', 'LendingController@dataLending');

Route::resource('movies', 'MoviesController');
Route::resource('members', 'MembersController');
Route::resource('lending', 'LendingController');